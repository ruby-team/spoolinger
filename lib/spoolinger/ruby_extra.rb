#--
# Spoolinger, a generic and lazy spool manager
# Copyright (c) 2012-2018 Marc Dequènes (Duck) <Duck@DuckCorp.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'active_support'

class Hash
  def to_ostruct
    data = self.dup
    data.each_pair do |k, v|
      data[k] = v.to_ostruct if v.is_a?(Hash)
    end
    OpenStruct.new(data)
  end
end

class OpenStruct
  def to_hash
    @table.dup
  end
end

class Object
  def logger
    Spoolinger::Logger.instance
  end

  def self.human_name
    self.name.split("::").last
  end
  def human_name
    self.class.human_name
  end
end

class String
  def is_numeric?
    Float self rescue false
  end
end
