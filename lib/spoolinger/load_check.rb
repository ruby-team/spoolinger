#--
# Spoolinger, a generic and lazy spool manager
# Copyright (c) 2012-2018 Marc Dequènes (Duck) <Duck@DuckCorp.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++


require 'open3'

module Spoolinger

  class LoadChecker
    def self.create(config)
      klass = case config.type
              when 'exec'
                LoadCheckerExec
              when 'ruby'
                LoadCheckerRuby
              end
      raise "Wrong load check method" if klass.nil?

      klass.new(config)
    end

    def initialize(config)
      @config = config
    end
  end

  class LoadCheckerExec < LoadChecker
    def check
      output = nil
      err = nil
      pstat = nil
      begin
        Open3.popen3(@config.script) do |stdin, stdout, stderr, wait_thr|
          output = stdout.read.strip
          err = stderr.read.strip
          pstat = wait_thr.value
        end

        if err
          if pstat.exitstatus != 0
            raise err
          else
            logger.warn "Load check script warnings: %s" % err
          end
        end
      rescue
        logger.error "Load check script failed: %s" % $!.to_s
        return
      end

      if ["0", "1"].include? output
        output == "1"
      else
        logger.error "Cannot understand load check script's output"
      end
    end
  end

  class LoadCheckerRuby < LoadChecker
    def initialize(config)
      super

      @m = Module.new
      begin
        content = IO.read(@config.script)
        @m.module_eval(content)
        @m.module_exec do
          module_function :check
        end
      rescue
        logger.error "Cannot load load check script: %s" % $!.to_s
        raise
      end
    end

    def check
      return @m.check
    rescue
      logger.error "Load check script failed: %s" % $!.to_s
    end
  end

end
