#--
# Spoolinger, a generic and lazy spool manager
# Copyright (c) 2012-2018 Marc Dequènes (Duck) <Duck@DuckCorp.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'singleton'
require 'yaml'
require 'kwalify'
require 'ostruct'

module Spoolinger
  class Config
    include Singleton

    CFG_DIR = File.join(RbConfig::CONFIG['sysconfdir'], 'spoolinger')
    CONFFILE_GLOBAL = "spoolinger.conf"
    DATA_DIR = File.join(RbConfig::CONFIG['datadir'], 'spoolinger')
    RUN_DIR = File.join(RbConfig::CONFIG['runstatedir'], 'spoolinger')
    BIN_DIR = RbConfig::CONFIG['bindir']

    def self.load
      # load all config parts
      g_config_raw = load_config_raw()
      g_def = parse('config', 'global', g_config_raw)
      g_default_config = load_default_config()
      config = merge_into_default_config(g_default_config, g_def)

      # create config object for easier access
      @@config = config.to_ostruct.freeze
    end

    def method_missing(sym, *args, &block)
      begin
        @@config.send sym, *args, &block
      rescue
        nil
      end
    end

    protected

    def self.merge_into_default_config(conf_default, conf)
      new_conf = conf_default.dup
      conf.each_pair do |k, v|
        if conf_default.has_key?(k) and conf_default[k].is_a?(Hash) and v.is_a?(Hash)
          new_conf[k] = merge_into_default_config(conf_default[k], v)
        elsif conf_default.has_key?(k) and conf_default[k].is_a?(Hash) and v.nil?
          new_conf[k] = conf_default[k]
        else
          new_conf[k] = v
        end
      end
      return new_conf
    end

    def self.load_config_raw
      begin
        filepath = File.join(CFG_DIR, CONFFILE_GLOBAL)
        File.read(filepath)
      rescue
        logger.fatal "Problems fetching configuration file '" + filepath + "': " + $!
        exit 1
      end
    end

    def self.parse(def_name, type, def_data_raw, validate = true, extra_data = nil)
      schema = load_schema(type)

      if validate
        validator_klass = case type
        when 'global'
          GlobalValidator
        else
          klass_name = type + "Validator"
          if self.const_defined? klass_name
             self.const_get klass_name
          else
            SpoolingerValidator
          end
        end
        validator = validator_klass.new(schema)
        validator.extra_data = extra_data
      else
        validator = nil
      end

      # validate config with schema
      parser = Kwalify::Yaml::Parser.new(validator)
      def_data = parser.parse(def_data_raw, :filename => def_name, :untabify => true)
      raise_if_validation_errors("#{type} '#{def_name}'", parser.errors)

      def_data
    rescue
      logger.fatal "Problems parsing %s file for '%s': %s" % [def_name, type, $!]
      exit 1
    end

    def self.load_default_config(type = 'global')
      filename = File.join(DATA_DIR, "default_config", type.downcase + ".yaml")

      return unless File.exists?(filename) or type == 'global'

      default_config_raw = File.read(filename)
      default_config = parse(':defaults:', type, default_config_raw, false)
      default_config || {}
    rescue
      logger.fatal "Could not load default config for '%s': %s" % [type, $!]
      exit 1
    end

    def self.load_schema(type = "global")
      schema_file = File.join(DATA_DIR, "schema", type.downcase + ".yaml")
      schema = YAML.load_file(schema_file)

      # validate schema
      metavalidator = Kwalify::MetaValidator.instance
      errors = metavalidator.validate(schema)
      raise_if_validation_errors("'#{type}' schema", errors)

      schema
    rescue
      logger.fatal "Could not load schema for '%s': %s" % [type, $!]
      exit 1
    end

    protected

    class SpoolingerValidator < Kwalify::Validator
      attr_accessor :extra_data

      ## hook method called by Validator#validate()
      def validate_hook(value, rule, path, errors)
        msg_list = []

        validate_hook_in(value, rule, path, msg_list)

        msg_list.each do |msg|
          errors << Kwalify::ValidationError.new(msg, path)
        end
      end

      def validate_hook_in(value, rule, path, msg_list)
      end
    end

    class GlobalValidator < SpoolingerValidator
      def validate_hook_in(value, rule, path, msg_list)
        case rule.name
        when 'LogPath'
          unless File.directory? value
            msg_list << "Log path must be an existing directory"
          end
        end
      end
    end

    def self.raise_if_validation_errors(name, errors)
      if errors and not errors.empty?
        err_msg = []
        for e in errors
          err_msg << "[#{e.path}] #{e.message}"
        end
        logger.fatal "%s is not valid:\n%s" % [name, err_msg.join("\n")]
        exit 1
      end
    end
  end
end
