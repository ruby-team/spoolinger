#--
# Spoolinger, a generic and lazy spool manager
# Copyright (c) 2012-2018 Marc Dequènes (Duck) <Duck@DuckCorp.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'rb-inotify'


# workaround for http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=589496
module INotify
  class Notifier
    class EINVAL < IOError
    end
  end
end

module Spoolinger
    class IncomingWatcher
      attr_reader :notifier
      attr_accessor :stopping, :stopped

      def initialize(path, &cb)
        begin
          @notifier = INotify::Notifier.new
          @watcher = @notifier.watch(path, :moved_to, :close_write) do |event|
            cb.call(event.absolute_name) unless event.name.empty?
          end
        rescue
          raise "Watch of directory '#{path}' failed: " + $!
        end

        EM.watch @notifier.to_io, WatchConn, self do |conn|
          conn.notify_readable = true
        end
      end

      def stop
        return if @stopping

        @stopping = true
        @watcher.close
        @stopped = true
      rescue SystemCallError
      end

      class WatchConn < EventMachine::Connection
        def initialize(watcher)
          @watcher = watcher

          super
        end

        def notify_readable
          return if @watcher.stopping

          @watcher.notifier.process
        end
      end
    end
end
