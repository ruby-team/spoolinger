#--
# Spoolinger, a generic and lazy spool manager
# Copyright (c) 2012-2018 Marc Dequènes (Duck) <Duck@DuckCorp.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

module Spoolinger
    class SpoolFile
      attr_reader :filename, :arrival_ts, :attrs, :key, :cancel_key

      def initialize(filename, options)
        @filename = filename

        @attrs = {}

        if options.file_name_pattern
          r = options.file_name_pattern.match(File.basename(filename))
          if r.nil?
            raise "pattern does not match filename"
          end
          add_regex_result_to_attrs(r)
        end

        if options.file_content_patterns
          File.open(filename) do |io|
            io.each_line do |line|
              begin
                options.file_content_patterns.each do |regex|
                  r = regex.match(line)
                  add_regex_result_to_attrs(r) if r
                end
              rescue
                # badly encoded file…
              end
            end
          end # File.open
        end

        @arrival_ts = @attrs['arrival_ts'] ? @attrs['arrival_ts'].to_i : Time.now.tv_sec

        options.file_keys.each do |file_key|
          key = template_replace(file_key)
          if key.index('%').nil?
            @key = key
            break
          end
        end
        if @key.nil?
          raise "file key could not be fully determined: %s" % @key
        end

        if options.cancel_key
          @cancel_key = template_replace(options.cancel_key)
          unless @cancel_key.index('%').nil?
            raise "cancel key could not be fully determined: %s" % @cancel_key
          end
        end
      end

      def delete
        File.delete(@filename)
      end

      def template_replace(str_orig)
        str = str_orig.dup

        @attrs.merge({
                       'filename' => @filename,
                       'arrival_ts' => @arrival_ts.to_s
                     }).each_pair do |k, v|
          str.gsub!("%" + k + "%", v)
        end

        str
      end

      protected

      def add_regex_result_to_attrs(r)
        values = r.captures
        new_attrs = r.names.inject({}) {|h, k| h[k] = values.shift; h }
        @attrs.merge! new_attrs
      end
    end
end
