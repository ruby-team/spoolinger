#--
# Spoolinger, a generic and lazy spool manager
# Copyright (c) 2012-2018 Marc Dequènes (Duck) <Duck@DuckCorp.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#++

require 'singleton'
require 'log4r'

module Spoolinger
  class Logger < Log4r::Logger
    include Singleton

    LOG_FORMAT = "[%5l - %d] #PREFIX#%m"

    def output_level(level)
      @main_outputter.level = level_to_log4r(level)
    end

    def log_to_file(filename, output_level = nil)
      file_outputter = Log4r::FileOutputter.new(filename, :filename => filename, :trunc => false)
      file_outputter.formatter = @default_formatter
      file_outputter.level = level_to_log4r(output_level) unless output_level.nil?
      self.outputters << file_outputter
    end

    def set_prefix(prefix = "")
      prefix ||= ""
      log_format = LOG_FORMAT.gsub("#PREFIX#", prefix)
      @default_formatter = Log4r::PatternFormatter.new(:pattern => log_format)
      self.outputters.each{|outputter| outputter.formatter = @default_formatter }
    end

    private

    def level_to_log4r(level)
      case level
      when 'verbose'
        Log4r::DEBUG
      when 'quiet'
        Log4r::ERROR
      else # normal
        Log4r::INFO
      end
    end

    def initialize
      super(PRODUCT)

      self.level = Log4r::DEBUG
      @main_outputter = Log4r::Outputter.stdout
      @main_outputter.level = Log4r::WARN
      self.outputters = [@main_outputter]

      set_prefix()
    end
  end
end
