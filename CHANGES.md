# CHANGES

## 0.3.1 (2021-10-07)

- replace obsolete setup.rb with a gemspec

## 0.3 (2018-06-18)

- daemon: fix stopping crashes and make it slightly faster
- systemd: add service file
- matching: `file_keys` list replaces `file_key` so you can provide fallback keys using different patterns
  (first key which is fully determined is selected)
- matching: allow case insensitive regex if `file_content_patterns_case_insensitive` is true
- doc: add Rspamd example
- a few other minor fixes

# 0.2.1 (2018-05-15)

- daemon: cleaned shebangs
- doc: updated install instructions

# 0.2

- matching: introduce file content pattern matching; exposed attributes have changed
- matching: `file_key` now defaults to the file name
- spool: `min_age` is now really optional
- spool: renamed `reschedule_period` setting into `reschedule_interval`
- spool: delete new file if a previous one with the same key exist
- doc: improved description and install instructions
- doc: add a complete dovecot-antispam + DSPAM example
- a few other minor fixes
