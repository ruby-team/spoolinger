
def check
  uptime = `uptime`.match(/([0-9.]+), ([0-9.]+), ([0-9.]+)$/)
  raise "could not fetch uptime data" if $?.exitstatus != 0 or uptime.nil?
  load = uptime[2].to_f

  load < 10
end
